# The "FOXTAR-market" project

## Quick start

Setup the repo and install dependencies:
```bash
$ git clone https://foster3290@bitbucket.org/foster3290/team-b.git
$ cd team-b && npm install
```
